# -*- coding: utf-8 -*-
"""
Created on Wed May 11 22:03:16 2022

@author: User
"""
import numpy as np
from numpy import sin, cos


class Psi:
    global m
    m = 1
    global hbar
    hbar = 1
    def __init__(
            self,
            omega,
            R_0 = 0,
            P_0 = 0,
            alpha_0 = 1,
            x_space = np.linspace(-5, 5, 512)
            ):
        
        self.x = x_space
        
        
        self.omega = omega
        self.alpha_0 = alpha_0
        self.R_0 = R_0
        self.P_0 = P_0
        
        self.t = np.linspace(0, 4 * np.pi / self.omega, 1024)
        self.V = m * self.omega**2 / 2 * self.x**2
        

        
        self.N = self.phi = self.alpha = self.R = self.P = np.empty(len(self.t))
        
        # parameter generation
        R = lambda t: self.R_0 * cos(self.omega * t) + self.P_0/(m * self.omega) * sin(self.omega * t)
        P = lambda t: self.P_0 * cos(self.omega * t) - self.R_0*self.omega * m * sin(self.omega * t)
        
        alpha = lambda t: (self.alpha_0 * cos(self.omega * t) + 1j * sin(self.omega * t)) / (cos(self.omega * t) + 1j * self.alpha_0 * sin(self.omega * t))
        
        constans = np.power(m * self.omega / (np.pi * hbar), 1/4)
        N = lambda t: constans * np.power(alpha(t), 1/4)
        
        tau_dot = lambda t: hbar * self.omega / 2 * np.real(alpha(t)) + P(t)**2 / (2 * m) - (m * self.omega**2) / 2 * R(t)**2
        # coś jest nie tak tutaj, ale idk co
        tau = self.integrate(self.t, tau_dot)
        
        self.test = tau_dot(self.t)
        
        
        self.phi = - tau / hbar
        self.N = N(self.t)
        self.alpha = alpha(self.t)
        self.R = R(self.t)
        self.P = P(self.t)
        
        
        
    def psi(self, i):
        
        psi = self.N[i] * np.exp(1j * self.phi[i] - m*self.omega/(2*hbar) * self.alpha[i] * (self.x - self.R[i])**2 + 1j * self.x * self.P[i] / hbar)
        return psi
        
    
    def print_normalizer(self, psi_complex):
        return psi_complex.real**2 + psi_complex.imag**2
    @staticmethod
    def integrate(x, fun):
        Fun = np.zeros(len(x), dtype = np.complex)
        Fun[0] = fun(x[0])
        for i in range(1, len(Fun)):
            Fun[i] = Fun[i - 1] + fun(x[i])
        return (Fun - Fun[0]) * (x[1] - x[0])
    @staticmethod
    def abs_pow2(num):
        return np.real(num)**2 * np.imag(num)**2
    
    @staticmethod
    def integral(psi, dx = 1):
        return np.real(np.sqrt(psi * np.conjugate(psi)).sum() * dx)