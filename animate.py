import matplotlib.pyplot as plt
from matplotlib import animation
from numpy import conjugate, pi

from psi import Psi

def generate_anim(T, y_lim = 1, frame_skip = 16):
    
    
    fig, ax1 = plt.subplots(figsize=(8, 6), dpi = 100)
    # plot background
    fig.patch.set_facecolor('white')
    
    ax1.set_xlim(min(T.x), max(T.x))
    ax1.set_ylim(-y_lim, y_lim)
    ax2 = ax1.twinx()
    ax2.set_ylim([0, T.V[0]])
    line, = ax1.plot([], [], lw=1)
    ax1.set_xlabel(r'$x$')
    ax1.set_ylabel(r'$\Psi(x)$')
    ax2.set_ylabel(r'$V(x)$')
    
    text_psi_data = (
                    r'$\Omega = ' + str(T.omega) 
                    + r'$ ' + r'$R_0 = ' + str(T.R_0) 
                    + r'$ ' + r'$\alpha_0 = ' + str(T.alpha_0)
                    + r'$'
                    )
    fig.suptitle(text_psi_data, fontsize=20)
    
    
    plotcols = ["green", "blue","red", "black"]
    labels = ["$V(X)$", "$\Re(\Psi)$", "$\Im(\Psi)$", "$|\Psi|^2$"]
    lines = []
    
    
    
    ax2.plot(T.x,T.V,lw=1,color=plotcols[0], label = labels[0])
    
    
    lobj_real = ax1.plot([],[],lw=1,color=plotcols[1], label = labels[1])[0]
    lines.append(lobj_real)
    lobj_imag = ax1.plot([],[],lw=1,color=plotcols[2], label = labels[2])[0]
    lines.append(lobj_imag)
    
    
    lobj_density = ax1.plot([],[],lw=1,color=plotcols[3], label = labels[3])[0]
    lines.append(lobj_density)
    
    ax1.legend(handles = lines,
               labels = labels[1:4],
               fontsize = 10)
    
    def init():
        for line in lines:
            line.set_data([],[])
        return lines
    
    
    #Data
    def animate(i):
        psi = T.psi(int(i * frame_skip))
    
        x = T.x
        ylist = [psi.real, psi.imag, (psi*conjugate(psi)).real]
    
        #for index in range(0,1):
        for lnum,line in enumerate(lines):
            line.set_data(x, ylist[lnum]) # set data for each line separately. 
    
        return lines
    
    
    
    # call the animator.  blit=True means only re-draw the parts that have changed.
    anim = animation.FuncAnimation(fig, animate, init_func=init,
                                   frames=int(len(T.t) / frame_skip), interval=10, blit=True)
    
    
    anim.save(r'anims/'+"anim"+(
                    str(T.omega) 
                    + r'_' + str(T.R_0) 
                    + r'_' + str(T.alpha_0)
                    )+".gif",
              writer = animation.writers['ffmpeg'](fps=30),
              dpi = 100)
    

