# praktyki2022

## How to use
First u need to create a Psi object
```
# importing class
from praktyki_psi_new import Psi

# creating Psi object with Omega = 1 (other parameters set to defaults)
T = Psi(1)
```

Now you can use the Psi object to create an animation/plot snapshots of the wavefunction

```
# to generate animation (.gif will be created in 'anims' folder)
generate_anim(T)

# to plot snapshots (.png will be created in 'plots' folder)
make_psi_plot(T)
```

Make sure you checkout optional arguments (they may need to be changed for your needs)

## Licence
Summer internship 2022 programm, some helpful files, animation software for visualization of Quantum harmonic oscillator (with classical and logarythmic hamiltonian)
    Copyright (C) 2022  Jan Rawa

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
