import numpy as np
from numpy import sin, cos

from psi import Psi

class Psi_log(Psi):
    global m
    m = 1
    global hbar
    hbar = 1
    def __init__(
            self,
            omega,
            R_0 = 0,
            P_0 = 0,
            alpha_0 = 1,
            b = 0,
            a = 1,
            x_space = np.linspace(-5, 5, 512)
            ):
        
        self.x = x_space
        
        
        self.omega = omega
        self.alpha_0 = alpha_0
        self.R_0 = R_0
        self.P_0 = P_0
        
        self.bp = b / hbar
        self.omega_prim = np.sqrt(self.bp**2 + self.omega**2)
        
        self.t = np.linspace(0, 4 * np.pi / self.omega, 1024)
        self.V = m * self.omega**2 / 2 * self.x**2
        

        
        self.N = self.phi = self.alpha = self.R = self.P = np.empty(len(self.t))
        
        # parameter generation
        R = lambda t: self.R_0 * cos(self.omega * t) + self.P_0/(m * self.omega) * sin(self.omega * t)
        P = lambda t: self.P_0 * cos(self.omega * t) - self.R_0*self.omega * m * sin(self.omega * t)
        
        alpha = lambda t: ((self.alpha_0 * (cos(self.omega_prim * t) + 1j * self.bp/self.omega_prim * sin(self.omega_prim * t)) + 1j * self.omega/self.omega_prim * sin(self.omega_prim * t))
                            / ((cos(self.omega_prim * t) - 1j * self.bp/self.omega_prim * sin(self.omega_prim * t)) + 1j * self.alpha_0 * self.omega/self.omega_prim * sin(self.omega_prim * t)))
        
        constans = np.power(m * self.omega / (np.pi * hbar), 1/4)
        N = lambda t: constans * np.power(alpha(t), 1/4)
        
        tau_dot = lambda t: hbar * self.omega / 2 * np.real(alpha(t)) + P(t)**2 / (2 * m) - (m * self.omega**2) / 2 * R(t)**2 + 2 * b * np.log(N(t)) - b * np.log(a)
        # coś jest nie tak tutaj, ale idk co
        tau = self.integrate(self.t, tau_dot)
        
        self.test = tau_dot(self.t)
        
        
        self.phi = - tau / hbar
        self.alpha = alpha(self.t)
        self.N = N(self.t)
        self.R = R(self.t)
        self.P = P(self.t)